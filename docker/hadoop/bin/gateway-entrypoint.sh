#!/bin/bash
/opt/hadoop/hadoop_install/bin/hdfs dfs -mkdir /user
/opt/hadoop/hadoop_install/bin/hdfs dfs -mkdir /user/hadoop
/opt/hadoop/hadoop_install/bin/hdfs dfs -mkdir /user/hadoop/libs
/opt/hadoop/hadoop_install/bin/hdfs dfs -put /opt/hadoop/spark_install/spark-bin-hadoop2.6-jars.tar /user/hadoop/libs
/opt/hadoop/hadoop_install/bin/hdfs dfs -mkdir /user/hadoop/app-logs/
/opt/hadoop/hadoop_install/bin/hdfs dfs -mkdir /user/hadoop/app-logs/spark
/opt/hadoop/hadoop_install/bin/hdfs dfs -mkdir /user/hadoop/app-logs/spark/logs
/opt/hadoop/hadoop_install/bin/hdfs dfs -put /opt/hadoop/data/soccer /user/hadoop/soccer