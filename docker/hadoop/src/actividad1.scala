import org.apache.spark.sql.SaveMode
spark.read.option("header", "true").option("multiLine", true).option("escape", "\"").csv("soccer/Country.csv").write.mode(SaveMode.Overwrite).saveAsTable("countries")
spark.read.option("header", "true").option("multiLine", true).option("escape", "\"").csv("soccer/League.csv").write.mode(SaveMode.Overwrite).saveAsTable("leagues")
spark.read.option("header", "true").option("multiLine", true).option("escape", "\"").csv("soccer/Match.csv").write.mode(SaveMode.Overwrite).saveAsTable("matches")
spark.read.option("header", "true").option("multiLine", true).option("escape", "\"").csv("soccer/Player.csv").write.mode(SaveMode.Overwrite).saveAsTable("players")
spark.read.option("header", "true").option("multiLine", true).option("escape", "\"").csv("soccer/Player_Attributes.csv").write.mode(SaveMode.Overwrite).saveAsTable("player_attributes")
spark.read.option("header", "true").option("multiLine", true).option("escape", "\"").csv("soccer/Team.csv").write.mode(SaveMode.Overwrite).saveAsTable("teams")
spark.read.option("header", "true").option("multiLine", true).option("escape", "\"").csv("soccer/Team_Attributes.csv").write.mode(SaveMode.Overwrite).saveAsTable("team_attibutes")