## Base para la Actividad 2

Para construir las imágenes de docker  e iniciar el clúster:

`docker-compose up --build`

Una vez esté encendido el clúster, eso carga los ficheros a HDFS y crea los directorios:

`docker exec -it --user hadoop il3-hadoop-gateway sh /opt/hadoop/bin/gateway-entrypoint.sh`

Finalmente esta línea es para convertir los ficheros CSV en tablas de Hive:

`docker exec -it --user hadoop il3-hadoop-gateway /opt/hadoop/spark_install/bin/spark-shell --master yarn -i /opt/hadoop/src/actividad1.scala`

Si se quiere entrar en la máquina de Gateway, dónde se pueden levantar spark-shells, o ejecutar jars:

`docker exec -it --user hadoop il3-hadoop-gateway bash`

Nota: Aseguraros de que le dais suficiente CPU y RAM a la máquina virtual de Docker. En mi configuración tengo 6 CPUs y 10GB de RAM
